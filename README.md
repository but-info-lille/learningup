# SAé 1.06 : RECUEIL DE BESOINS
> - Antoine AMELOOT
> - Valentin DASSONVILLE  

> Septembre 2021

## Description

Projet de première année de BUT Informatique visant à créer et fournir une analyse sémiotique du site vitrine d'une start-up fictive.

L'entreprise Learning'Up fournit des logiciels éducatifs mêlant langues et sciences. Le public visé est l'ensemble des collèges et lycées francophones.


## Informations additionnelles

- Le site a été intégralement passé dans le W3 Validator et des avertissements ont été relevés.
  - Avertissement sur l'absence de titre pour les sections (ces sections n'ont volontairement pas de noms mais en nécessitent du point de vue du validator).
  - Erreurs sur les propriétés CSS des kits des icônes FontAwesome.